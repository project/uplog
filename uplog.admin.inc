<?php
/**
 * @file
 * Render the Update log overview table and the Update log detail table.
 */

/**
 * Menu callback; Display a table of Update logs.
 */
function uplog_overview() {
  $types = uplog_types();
  $rows = array();
  $header = array(
    array('data' => t('Date'), 'field' => 'up.upid', 'sort' => 'desc'),
    array('data' => t('User'), 'field' => 'u.name'),
    array('data' => t('Module'), 'field' => 'up.module'),
    array('data' => t('Schema version'), 'field' => 'up.version'),
    array('data' => t('Type'), 'field' => 'up.type'),
  );

  $query = db_select('uplog', 'up')
    ->extend('PagerDefault')
    ->extend('TableSort');
  $query->leftJoin('users', 'u', 'up.uid = u.uid');
  $query->fields('up', array(
    'upid',
    'uid',
    'module',
    'version',
    'status',
    'type',
    'timestamp',
  ));
  $query->fields('u', array('name'));

  if (!empty($_SESSION['uplog_overview_filter']['modules'])) {
    $query->condition('up.module', $_SESSION['uplog_overview_filter']['modules'], 'IN');
  }
  if (!empty($_SESSION['uplog_overview_filter']['type'])) {
    $query->condition('up.type', $_SESSION['uplog_overview_filter']['type']);
  }
  if (isset($_SESSION['uplog_overview_filter']['status']) && is_numeric($_SESSION['uplog_overview_filter']['status'])) {
    $query->condition('up.status', $_SESSION['uplog_overview_filter']['status']);
  }
  $result = $query->limit(50)->orderByHeader($header)->execute();

  foreach ($result as $uplog) {
    $module = system_get_info('module', $uplog->module);
    $rows[] = array(
      'data' => array(
        array(
          'data' => l(format_date($uplog->timestamp, 'short'), 'admin/reports/uplog/' . $uplog->upid),
        ),
        array(
          'data' => theme('username', array('account' => $uplog)),
        ),
        array(
          'data' => isset($module['name']) ? $module['name'] : '',
        ),
        array(
          'data' => check_plain($uplog->version),
        ),
        array(
          'data' => isset($types[$uplog->type]) ? $types[$uplog->type]['label'] : '',
        ),
      ),
      'class' => $uplog->status == UPLOG_SUCCESS ? array('uplog-success') : array('uplog-failed'),
    );
  }

  return array(
    'uplog_filter_form' => drupal_get_form('uplog_filter_form'),
    'uplog_clear_form' => drupal_get_form('uplog_clear_form'),
    'uplog_table' => array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => array('id' => 'admin-uplog'),
      '#empty' => t('No update log messages available.'),
      '#attached' => array(
        'css' => array(drupal_get_path('module', 'uplog') . '/uplog.css'),
      ),
    ),
    'uplog_pager' => array('#theme' => 'pager'),
  );
}

/**
 * Form constructor for the Update log filter form.
 *
 * @see uplog_overview()
 */
function uplog_filter_form($form, &$form_state) {
  $form['uplog_filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter log messages'),
    '#collapsible' => TRUE,
  );

  $modules = uplog_modules();
  if (!empty($modules)) {
    $form['uplog_filters']['modules'] = array(
      '#type' => 'select',
      '#title' => t('Modules'),
      '#options' => $modules,
      '#multiple' => TRUE,
      '#size' => 8,
      '#default_value' => isset($_SESSION['uplog_overview_filter']['modules']) ? $_SESSION['uplog_overview_filter']['modules'] : array(),
    );
  }

  $types = array();
  foreach (uplog_types() as $key => $type) {
    $types[$key] = $type['label'];
  }
  $form['uplog_filters']['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => $types,
    '#empty_option' => t('- All -'),
    '#default_value' => isset($_SESSION['uplog_overview_filter']['type']) ? $_SESSION['uplog_overview_filter']['type'] : '',
  );

  $form['uplog_filters']['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => array(
      UPLOG_SUCCESS => t('Success'),
      UPLOG_FAILED => t('Failed'),
    ),
    '#empty_option' => t('- All -'),
    '#default_value' => isset($_SESSION['uplog_overview_filter']['status']) ? $_SESSION['uplog_overview_filter']['status'] : '',
  );

  $form['uplog_filters']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );

  $form['uplog_filters']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );

  if (!empty($_SESSION['uplog_overview_filter'])) {
    $form['uplog_filters']['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
    );
  }

  return $form;
}

/**
 * Form submission handler for uplog_filter_form().
 */
function uplog_filter_form_submit($form, &$form_state) {
  switch ($form_state['values']['op']) {
    case t('Filter'):
      $_SESSION['uplog_overview_filter']['modules'] = isset($form_state['values']['modules']) ? $form_state['values']['modules'] : array();
      $_SESSION['uplog_overview_filter']['type'] = isset($form_state['values']['type']) ? $form_state['values']['type'] : '';
      $_SESSION['uplog_overview_filter']['status'] = isset($form_state['values']['status']) ? $form_state['values']['status'] : '';
      break;

    case t('Reset'):
      $_SESSION['uplog_overview_filter'] = array();
      break;
  }
}

/**
 * Form constructor for the form that clears out the Update log.
 *
 * @see uplog_overview()
 */
function uplog_clear_form($form, &$form_state) {
  $form['uplog_clear'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clear log messages'),
    '#description' => t('This will permanently remove the update log entries from the database.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['uplog_clear']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear log messages'),
  );

  return $form;
}

/**
 * Form submission handler for uplog_clear_form().
 */
function uplog_clear_form_submit($form, &$form_state) {
  db_delete('uplog')->execute();
  drupal_set_message(t('Update log cleared.'));
  watchdog('uplog', 'Cleared Update log');
}

/**
 * Menu callback; Display details of a specific Update log entry.
 *
 * @return object $uplog
 *   Loaded Update log entry.
 *
 * @see uplog_load()
 */
function uplog_details($uplog) {
  $module = system_get_info('module', $uplog->module);
  $types = uplog_types();

  return array(
    '#theme' => 'table',
    '#header' => array(),
    '#rows' => array(
      array(
        array('header' => TRUE, 'data' => t('Module')),
        $module['name'],
      ),
      array(
        array('header' => TRUE, 'data' => t('Date')),
        format_date($uplog->timestamp, 'long'),
      ),
      array(
        array('header' => TRUE, 'data' => t('User')),
        theme('username', array('account' => $uplog)),
      ),
      array(
        array('header' => TRUE, 'data' => t('Schema version')),
        check_plain($uplog->version),
      ),
      array(
        array('header' => TRUE, 'data' => t('Type')),
        isset($types[$uplog->type]) ? $types[$uplog->type]['label'] : '',
      ),
      array(
        array('header' => TRUE, 'data' => t('Status')),
        $uplog->status == UPLOG_SUCCESS ? t('Success') : t('Failed'),
      ),
      array(
        array('header' => TRUE, 'data' => t('Description')),
        filter_xss($uplog->description),
      ),
      array(
        array('header' => TRUE, 'data' => t('Message')),
        filter_xss($uplog->message, array()),
      ),
    ),
    '#attributes' => array('id' => 'admin-uplog'),
    '#empty' => t('Update log details could not be found.'),
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'uplog') . '/uplog.css'),
    ),
  );
}
