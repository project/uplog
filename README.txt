CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Update log module provides an overview to all updates, providing
information of each update. It will log each update that is processed via Drush
or update.php.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/uplog

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/search/uplog


REQUIREMENTS
------------

 * No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7 for
   further information.


CONFIGURATION
-------------

 * Users in roles with the "Administer site configuration" permissions will be
   able to view the Update log overview via Reports » Update log.


MAINTAINERS
-----------

Current maintainers:
 * Mitch Portier (Arkener) - http://drupal.org/user/2284182
