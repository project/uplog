<?php
/**
 * @file
 * Update log drush integration.
 */

/**
 * Implements hook_drush_help().
 */
function uplog_drush_help($section) {
  switch ($section) {
    case 'drush:uplog-list':
      return dt('Show available Update log entries.');
    case 'drush:uplog-show':
      return dt('Show Update log entry in detail.');
  }
}

/**
 * Implements hook_drush_command().
 */
function uplog_drush_command() {
  $items['uplog-list'] = array(
    'description' => dt('Show available Update log entries.'),
    'drupal dependencies' => array('uplog'),
    'aliases' => array('upl', 'up-list'),
    'options' => array(
      'count' => dt('The number of entries to show. Defaults to 10.'),
    ),
    'examples' => array(
      'drush uplog-list' => dt('Show a listing of most recent 10 entries.'),
      'drush uplog-list --count=50' => dt('Show a listing of most recent 50 entries.'),
    ),
  );
  $items['uplog-show'] = array(
    'description' => dt('Show Update log entry in detail.'),
    'drupal dependencies' => array('uplog'),
    'aliases' => array('ups', 'up-show'),
    'arguments' => array(
      'upid' => dt('ID of a Update log entry to show in detail.'),
    ),
    'examples' => array(
      'drush uplog-show 93' => dt('Show Update log entry with id 93 in detail.'),
    ),
  );
  return $items;
}

/**
 * Callback function for uplog-list command.
 */
function drush_uplog_list() {
  $count = drush_get_option('count', 10);
  $fields = array('upid', 'module', 'timestamp', 'version', 'type', 'status');
  $types = drush_uplog_types();
  $result = drush_db_select('uplog', $fields, NULL, NULL, 0, $count, 'upid', 'DESC');

  $table = array();
  $header = array(
    dt('ID'),
    dt('Module'),
    dt('Date'),
    dt('Schema version'),
    dt('Type'),
    dt('Status'),
  );

  foreach ($result as $uplog) {
    $date = !empty($uplog->timestamp) ? format_date($uplog->timestamp, 'custom', 'd/M H:i') : NULL;
    $status = $uplog->status == UPLOG_SUCCESS ? dt('Success') : dt('Failed');
    $table[] = array(
      $uplog->upid,
      $uplog->module,
      $date,
      $uplog->version,
      (isset($types[$uplog->type]) ? $types[$uplog->type]['label'] : ''),
      $status,
    );
  }

  if (empty($table)) {
    drush_log(dt('No log entries available.'), 'ok');
  }
  else {
    array_unshift($table, $header);
    drush_print_table($table, TRUE);
  }
}

/**
 * Callback function for uplog-show command.
 */
function drush_uplog_show($upid) {
  $fields = array('upid', 'module', 'timestamp', 'version', 'type', 'status', 'description', 'message');
  $types = drush_uplog_types();
  $result = drush_db_select('uplog', $fields, 'upid = :upid', array('upid' => $upid), 0, 1);
  $uplog = drush_db_fetch_object($result);

  if (!$uplog) {
    return drush_set_error(dt('Update log entry #%uplog could not be found.', array('%uplog' => $uplog)));
  }

  $table = array(
    dt('ID') => $uplog->upid,
    dt('Module') => $uplog->module,
    dt('Date') => format_date($uplog->timestamp, 'custom', 'd/M H:i'),
    dt('Schema version') => $uplog->version,
    dt('Type') => isset($types[$uplog->type]) ? $types[$uplog->type]['label'] : '',
    dt('Status') => ($uplog->status == UPLOG_SUCCESS ? dt('Success') : dt('Failed')),
    dt('Description') => $uplog->description,
    dt('Message') => $uplog->message,
  );

  drush_print_table(drush_key_value_to_array_table($table));
}

/**
 * Build a list of supported update functions.
 *
 * This is a clone of uplog_types() with t() replaced by dt().
 *
 * @return array
 *   List of Update log types, including the update callbacks and a label.
 *
 * @see uplog_types()
 */
function drush_uplog_types() {
  return array(
    'ui' => array(
      'function' => 'update_finished',
      'label' => dt('User interface'),
    ),
    'drush' => array(
      'function' => 'drush_update_finished',
      'label' => dt('Drush'),
    ),
  );
}